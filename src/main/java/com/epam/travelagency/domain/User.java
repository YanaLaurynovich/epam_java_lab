package com.epam.travelagency.domain;

import java.util.Objects;

/**
 * User entity.
 */
public class User extends Entity<Integer> {

    /**
     * User's login.
     */
    private String login;

    /**
     * User's password.
     */
    private String password;

    /**
     * Constructor without params.
     */
    public User() {
    }

    /**
     * Get user's login.
     *
     * @return login.
     */
    public final String getLogin() {
        return login;
    }

    /**
     * Set user's login.
     *
     * @param newLogin new user's login.
     */
    public final void setLogin(final String newLogin) {
        login = newLogin;
    }

    /**
     * Get user's password.
     *
     * @return password.
     */
    public final String getPassword() {
        return password;
    }

    /**
     * Set user's password.
     *
     * @param newPassword new user's password.
     */
    public final void setPassword(final String newPassword) {
        password = newPassword;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(getId(), login, password);
    }

    @Override
    public final boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        User user = (User) obj;
        return user.getId().equals(this.getId())
                && user.getLogin().equals(login)
                && user.getPassword().equals(password);
    }

    @Override
    public final String toString() {
        return String.format("Class %s, id %d, login %s, password %s",
                User.class.getSimpleName(), getId(), login, password);
    }
}
