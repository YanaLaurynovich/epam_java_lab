package com.epam.travelagency.domain;

/**
 * Enum of tour's type.
 */
public enum TourType {
    /**
     * Bus tour.
     */
    BUS("bus"),

    /**
     * Bike tour.
     */
    BIKE("bike"),

    /**
     * Gastronomic tour.
     */
    GASTRONOMIC("gastronomic"),

    /**
     * Cruises tour.
     */
    CRUISES("cruises"),

    /**
     * Winter tour.
     */
    WINTER("winter"),

    /**
     * Wedding tour.
     */
    WEDDING("wedding"),

    /**
     * Weekend tour.
     */
    WEEKEND("weekend"),

    /**
     * Shop tour.
     */
    SHOP("shop"),

    /**
     * Extreme tour.
     */
    EXTREME("extreme"),

    /**
     * Sightseeing tour.
     */
    SIGHTSEEING("sightseeing"),

    /**
     * Child tour.
     */
    CHILD("child"),

    /**
     * Educational tour.
     */
    EDUCATIONAL("educational"),

    /**
     * Medical tour.
     */
    MEDICAL("medical"),

    /**
     * Pilgrim tour.
     */
    PILIGRIM("pilgrim"),

    /**
     * Expedition tour.
     */
    EXPEDITION("expedition"),

    /**
     * City tour.
     */
    CITY("city");

    /**
     * Constructor for tour's type.
     * @param value tourType's string value.
     */

    TourType(final String value) {
        this.stringValue = value;
    }

    /**
     * TourType's string value.
     */
    private String stringValue;

    /**
     * Get tourType's string value.
     *
     * @return stringValue.
     */
    public String getStringValue() {
        return stringValue;
    }
}
