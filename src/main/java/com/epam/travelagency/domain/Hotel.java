package com.epam.travelagency.domain;

import java.util.List;
import java.util.Objects;

/**
 * Hotel entity.
 */
public class Hotel extends Entity<Integer> {

    /**
     * Hotel's name.
     */
    private String name;

    /**
     * Hotel's stars.
     */
    private int stars;

    /**
     * Hotel's website.
     */
    private String website;

    /**
     * Hotel's latitude in degrees.
     */
    private double latitude;

    /**
     * Hotel's longitude in degrees.
     */
    private double longitude;

    /**
     * Hotel's features.
     */
    private List<Feature> features;

    /**
     * Constructor without params.
     */
    public Hotel() {
    }

    /**
     * Get hotel's name.
     *
     * @return name.
     */
    public final String getName() {
        return name;
    }

    /**
     * Set hotel's name.
     *
     * @param newName new hotel's name.
     */
    public final void setName(final String newName) {
        name = newName;
    }

    /**
     * Get hotel's stars.
     *
     * @return stars.
     */
    public final int getStars() {
        return stars;
    }

    /**
     * Set hotel's stars.
     *
     * @param newStars new hotel's stars.
     */
    public final void setStars(final int newStars) {
        stars = newStars;
    }

    /**
     * Get hotel's website.
     *
     * @return website.
     */
    public final String getWebsite() {
        return website;
    }

    /**
     * Set hotel's website.
     *
     * @param newWebsite new hotel's website.
     */
    public final void setWebsite(final String newWebsite) {
        website = newWebsite;
    }

    /**
     * Get hotel's latitude.
     *
     * @return latitude.
     */
    public final double getLatitude() {
        return latitude;
    }

    /**
     * Set hotel's latitude.
     *
     * @param newLatitude new hotel's latitude.
     */
    public final void setLatitude(final double newLatitude) {
        latitude = newLatitude;
    }

    /**
     * Get hotel's longitude.
     *
     * @return longitude.
     */
    public final double getLongitude() {
        return longitude;
    }

    /**
     * Set hotel's longitude.
     *
     * @param newLongitude new hotel's longitude.
     */
    public final void setLongitude(final double newLongitude) {
        longitude = newLongitude;
    }

    /**
     * Get hotel's features.
     *
     * @return features.
     */
    public final List<Feature> getFeatures() {
        return features;
    }

    /**
     * Set hotel's features.
     *
     * @param newFeatures new hotel's features.
     */
    public final void setFeatures(final List<Feature> newFeatures) {
        features = newFeatures;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(getId(), name, stars, website, latitude, longitude,
                features);
    }

    @Override
    public final boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Hotel hotel = (Hotel) obj;
        return hotel.getId().equals(this.getId())
                && hotel.getName().equals(name)
                && hotel.getStars() == stars
                && hotel.getWebsite().equals(website)
                && (Math.abs(hotel.getLatitude() - latitude) <= 1E-5)
                && (Math.abs(hotel.getLongitude() - longitude) <= 1E-5)
                && hotel.getFeatures().equals(features);
    }

    @Override
    public final String toString() {
        return String.format("Class %s id %d, name %s, stars %d, website %s, "
                + "latitude %f, longitude %f, features %s",
                Hotel.class.getSimpleName(), getId(), name, stars, website,
                latitude, longitude, features);
    }
}
