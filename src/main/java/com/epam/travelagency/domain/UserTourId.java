package com.epam.travelagency.domain;

import java.util.Objects;

/**
 * User's tour entity.
 */
public class UserTourId {

    /**
     * User's login.
     */
    private Integer userId;

    /**
     * Tour's date.
     */
    private Integer tourId;

    /**
     * Constructor without params.
     */
    public UserTourId() {
    }

    /**
     * Get user's id.
     *
     * @return id.
     */
    public final Integer getUserId() {
        return userId;
    }

    /**
     * Set user's id.
     *
     * @param newUserId new user's id.
     */
    public final void setUserId(final Integer newUserId) {
        userId = newUserId;
    }

    /**
     * Get tour's id.
     *
     * @return id.
     */
    public final Integer getTourId() {
        return tourId;
    }

    /**
     * Set tour's id.
     *
     * @param newTourId new tour's id.
     */
    public final void setTourId(final Integer newTourId) {
        tourId = newTourId;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(userId, tourId);
    }

    @Override
    public final boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        UserTourId userTourId = (UserTourId) obj;
        return userTourId.getUserId().equals(userId)
                && userTourId.getTourId().equals(tourId);
    }

    @Override
    public final String toString() {
        return String.format("Class %s, user's id %d, tour's id %d",
                UserTourId.class.getSimpleName(), userId, tourId);
    }
}
