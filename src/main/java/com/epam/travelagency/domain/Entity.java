package com.epam.travelagency.domain;

import java.util.Objects;

/**
 * Entity class.
 *
 * @param <K> type of id.
 */
public class Entity<K> {

    /**
     * Entity's id.
     */
    private K id;

    /**
     * Get entity's id.
     *
     * @return id.
     */
    public final K getId() {
        return id;
    }

    /**
     * Set entity's id.
     *
     * @param newId new entity's id.
     */
    public final void setId(final K newId) {
        id = newId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Entity entity = (Entity) obj;
        return entity.getId().equals(id);
    }

    @Override
    public String toString() {
        return String.format("Class %s, id %s",
                Entity.class.getSimpleName(), id);
    }
}
