package com.epam.travelagency.domain;


import java.sql.Date;
import java.util.Objects;

/**
 * Review entity.
 */
public class Review extends Entity<Integer> {

    /**
     * Review's date.
     */
    private Date date;

    /**
     * Review's text.
     */
    private String text;

    /**
     * User's id {@link User}.
     */
    private Integer userId;

    /**
     * Tour's id {@link Tour}.
     */
    private Integer tourId;

    /**
     * Constructor without params.
     */
    public Review() {
    }

    /**
     * Get review's date.
     *
     * @return date.
     */
    public final Date getDate() {
        return date;
    }

    /**
     * Set review's date.
     *
     * @param newDate new review's date.
     */
    public final void setDate(final Date newDate) {
        date = newDate;
    }

    /**
     * Get review's text.
     *
     * @return text.
     */
    public final String getText() {
        return text;
    }

    /**
     * Set review's text.
     *
     * @param newText new review's text.
     */
    public final void setText(final String newText) {
        text = newText;
    }

    /**
     * Get user's id.
     *
     * @return userId.
     */
    public final Integer getUserId() {
        return userId;
    }

    /**
     * Set user's id.
     *
     * @param newUserId new user's id.
     */
    public final void setUserId(final Integer newUserId) {
        userId = newUserId;
    }

    /**
     * Get tour's id.
     *
     * @return tourId.
     */
    public final Integer getTourId() {
        return tourId;
    }

    /**
     * Set tour's id.
     *
     * @param newTourId new tour's id.
     */
    public final void setTourId(final Integer newTourId) {
        tourId = newTourId;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(getId(), text, date, tourId, userId);
    }

    @Override
    public final boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Review review = (Review) obj;
        return review.getId().equals(this.getId())
                && review.getText().equals(text)
                && review.getDate().equals(date)
                && review.getTourId() == tourId
                && review.getUserId() == userId;
    }

    @Override
    public final String toString() {
        return String.format("Class %s, id %d, text %s, date %s, tour's id %d,"
                        + " user's id %d", Review.class.getSimpleName(),
                getId(), text, date, tourId, userId);
    }
}
