package com.epam.travelagency.domain;

import java.util.Objects;

/**
 * Country entity.
 */
public class Country extends Entity<Integer> {

    /**
     * Country's name.
     */
    private String name;

    /**
     * Constructor without params.
     */
    public Country() {
    }

    /**
     * Get county's name.
     *
     * @return name.
     */
    public final String getName() {
        return name;
    }

    /**
     * Set countr's name.
     *
     * @param newName new country's name.
     */
    public final void setName(final String newName) {
        name = newName;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(getId(), name);
    }

    @Override
    public final boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Country country = (Country) obj;
        return country.getId().equals(this.getId())
                && country.getName().equals(name);
    }

    @Override
    public final String toString() {
        return String.format("Class %s, id %d, name %s",
                Country.class.getSimpleName(), getId(), name);
    }
}
