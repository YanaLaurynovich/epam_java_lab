package com.epam.travelagency.domain;

/**
 * Enum of hotel's features.
 */
public enum Feature {

    /**
     * Free smoke.
     */
    FREE_SMOKE("free smoke"),

    /**
     * Waterpark.
     */
    WATERPARK("waterpark"),

    /**
     * Free internet.
     */
    FREE_INTERNET("free internet"),

    /**
     * No pets.
     */
    NO_PETS("no pets"),

    /**
     * Free parking.
     */
    FREE_PARKING("free parking"),

    /**
     * Air conditioned.
     */
    AIR_CONDITIONED("air conditioned"),

    /**
     * Children friendly.
     */
    CHILDREN_FRIENDLY("children friendly"),

    /**
     * Non-smoking.
     */
    NON_SMOKING("non-smoking"),

    /**
     * In-room safes.
     */
    IN_ROOM_SAFES("in-room safes"),

    /**
     * On-site bike rentals.
     */
    ON_SITE_BIKE_RENTALS("on-site bike rentals");

    /**
     * Constructor for feature.
     *
     * @param stringValue string value of feature.
     */
    Feature(final String stringValue) {
        this.stingValue = stringValue;
    }

    /**
     * Feature's string value.
     */
    private String stingValue;

    /**
     * Get feature's string value.
     *
     * @return stringValue.
     */
    public String getStingValue() {
        return stingValue;
    }
}
