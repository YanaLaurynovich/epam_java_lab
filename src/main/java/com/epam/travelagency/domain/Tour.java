package com.epam.travelagency.domain;


import java.sql.Date;
import java.util.Objects;

/**
 * Tour entity.
 */
public class Tour extends Entity<Integer> {

    /**
     * Tour's photo.
     */
    private String photo;

    /**
     * Tour's date.
     */
    private Date date;

    /**
     * Tour's duration.
     */
    private int duration;

    /**
     * Tour's description.
     */
    private String description;

    /**
     * Tour's cost.
     */
    private double cost;

    /**
     * Hotel's id.
     */
    private long hotelId;

    /**
     * Country's id.
     */
    private long countryId;

    /**
     * Tour's type.
     */
    private TourType type;

    /**
     * Constructor without params.
     */
    public Tour() {
    }

    /**
     * Get tour's photo.
     * @return photo.
     */
    public final String getPhoto() {
        return photo;
    }

    /**
     * Set tour's photo.
     * @param newFoto new tour's photo.
     */
    public final void setPhoto(final String newFoto) {
        photo = newFoto;
    }

    /**
     * Get tour's date.
     * @return date.
     */
    public final Date getDate() {
        return date;
    }

    /**
     * Set tour's date.
     * @param newDate new tour's date.
     */
    public final void setDate(final Date newDate) {
        date = newDate;
    }

    /**
     * Get tour's duration.
     * @return duration.
     */
    public final int getDuration() {
        return duration;
    }

    /**
     * Set tour's duration.
     * @param newDuration new tour's duration.
     */
    public final void setDuration(final int newDuration) {
        duration = newDuration;
    }

    /**
     * Get tour's description.
     * @return description.
     */
    public final String getDescription() {
        return description;
    }

    /**
     * Set tour's description.
     * @param newDescription new tour's description.
     */
    public final void setDescription(final String newDescription) {
        description = newDescription;
    }

    /**
     * Get tour's cost.
     * @return cost.
     */
    public final double getCost() {
        return cost;
    }

    /**
     * Set tour's cost.
     * @param newCost new tour's cost.
     */
    public final void setCost(final double newCost) {
        cost = newCost;
    }

    /**
     * Get hotel's id.
     * @return hotelId.
     */
    public final long getHotelId() {
        return hotelId;
    }

    /**
     * Set hotel's id.
     * @param newHotelId new hotel's id.
     */
    public final void setHotelId(final long newHotelId) {
        hotelId = newHotelId;
    }

    /**
     * Get country's id.
     * @return countryId.
     */
    public final long getCountryId() {
        return countryId;
    }

    /**
     * Set country's id.
     * @param newCountryId new country's id.
     */
    public final void setCountryId(final long newCountryId) {
        countryId = newCountryId;
    }

    /**
     * Get tour's type.
     * @return type.
     */
    public final TourType getType() {
        return type;
    }

    /**
     * Set tour's type.
     * @param newType new tour's type.
     */
    public final void setType(final TourType newType) {
        type = newType;
    }

    @Override
    public final int hashCode() {
        return Objects.hash(getId(), photo, date, duration, description, cost,
                countryId, hotelId, type);
    }

    @Override
    public final boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Tour tour = (Tour) obj;
        return tour.getId().equals(this.getId())
                && tour.getPhoto().equals(photo)
                && tour.getDate().equals(date)
                && tour.getDuration() == duration
                && tour.getDescription().equals(description)
                && tour.getCost() == cost
                && tour.getCountryId() == countryId
                && tour.getHotelId() == hotelId
                && tour.getType().equals(type);
    }

    @Override
    public final String toString() {
        return String.format("Class %s, id %d, photo %s, date %s, duration %d,"
                + " description %s, cost %f, country's id %d, hotel's id %d,"
                + " type %s", Tour.class.getSimpleName(), getId(), photo, date,
                duration, description, cost, countryId, hotelId, type);
    }
}
