package com.epam.travelagency.repository;

import com.epam.travelagency.config.DomainConfig;
import com.epam.travelagency.domain.Entity;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

/**
 * Interface providers CRUD operations for classes extend Entity.
 *
 * @param <T> class extends Entity.
 * @param <K> type of id.
 */
public interface EntityRepository<T extends Entity, K> {

    /**
     * Context provides domain entity.
     */
    ApplicationContext APPLICATION_CONTEXT =
            new AnnotationConfigApplicationContext(DomainConfig.class);


    /**
     * Create new row with entity in the table, if there is no such entity.
     * Else update existing entity.
     *
     * @param entity created entity.
     * @return entity's id.
     */
    K save(final T entity);

    /**
     * Select all entities from table.
     *
     * @return list of entities.
     */
    List<T> findAll();

    /**
     * Select an entity according to the entity's id.
     *
     * @param id entity's id.
     * @return selected entity.
     */
    T find(final K id);

    /**
     * Delete entity from table according to the entity's id.
     *
     * @param id entity's id.
     * @return count of deleted entities.
     */
    int delete(final K id);

}
