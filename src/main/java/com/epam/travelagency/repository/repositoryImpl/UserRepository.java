package com.epam.travelagency.repository.repositoryImpl;

import com.epam.travelagency.domain.User;
import com.epam.travelagency.repository.EntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for user entity.
 */
@Repository
public class UserRepository implements EntityRepository<User, Integer> {

    /**
     * User's id.
     */
    private static final String ID = "id";

    /**
     * User's login.
     */
    private static final String LOGIN = "login";

    /**
     * User's password.
     */
    private static final String PASSWORD = "password";

    /**
     * RowMapper for user.
     */

    private static final RowMapper<User> ROW_MAPPER = (resultSet, i) -> {
        User user = APPLICATION_CONTEXT.getBean(User.class);
        user.setId(resultSet.getInt(ID));
        user.setLogin(resultSet.getString(LOGIN));
        user.setPassword(resultSet.getString(PASSWORD));
        return user;
    };

    /**
     * JdbcTemplate.
     */
    private final JdbcTemplate jdbcTemplate;

    /**
     * Logger.
     */
    private static Logger logger =
            LoggerFactory.getLogger(UserRepository.class);

    /**
     * Script to insert user in the "User" table.
     */
    private static final String INSERT_USER =
            "INSERT INTO \"User\" (login, password) VALUES (?, ?) RETURNING id";

    /**
     * Script to update user in the "User" table.
     */
    private static final String UPDATE_USER =
            "UPDATE \"User\" SET login = ?, password = ? WHERE id = ?";

    /**
     * Script to delete user from the "User" table.
     */
    private static final String DELETE_USER =
            "DELETE FROM \"User\" WHERE id = ?";

    /**
     * Script to find all users in the "User" table.
     */
    private static final String FIND_ALL_USERS =
            "SELECT id, login, password FROM \"User\"";

    /**
     * Script to find user by id in the "User" table.
     */
    private static final String FIND_USER =
            "SELECT id, login, password FROM \"User\" WHERE id= ?";

    /**
     * Constructor.
     *
     * @param template JdbcTemplate.
     */
    @Autowired
    public UserRepository(final JdbcTemplate template) {
        this.jdbcTemplate = template;
    }

    /**
     * Create new row with user in the "User" table, if there is no such user.
     * Else update existing user.
     *
     * @param user saved user.
     * @return  id saved user.
     */
    @Override
    public final Integer save(final User user) {
        Integer id;
        String login = user.getLogin();
        String password = user.getPassword();
        if (user.getId() == null) {
            id = jdbcTemplate.queryForObject(INSERT_USER, Integer.class,
                    login, password);
            logger.debug("Insert new user with login = " + login);

        } else {
            id = user.getId();
            jdbcTemplate.update(UPDATE_USER, login, password, id);
            logger.debug("Update user with id = " + id);
        }
        return id;
    }

    /**
     * Select all users from the "User" table.
     *
     * @return list of users.
     */
    @Override
    public final List<User> findAll() {
        List<User> users = jdbcTemplate.query(FIND_ALL_USERS, ROW_MAPPER);
        logger.debug("Find all users! " + users);
        return users;
    }

    /**
     * Select a user according to the user's id in "User" table.
     *
     * @param id user's id.
     * @return selected user.
     */
    @Override
    public final User find(final Integer id) {
        User user = jdbcTemplate.queryForObject(
                FIND_USER, new Object[]{id}, ROW_MAPPER);
        logger.debug("Find a user. " + user);
        return user;
    }

    /**
     * Delete user from the "User" table according to the user's id.
     *
     * @param id user's id.
     */
    @Override
    public final int delete(final Integer id) {
        int count = jdbcTemplate.update(DELETE_USER, id);
        logger.debug("Delete user with id = " + id);
        return count;
    }


}
