package com.epam.travelagency.repository.repositoryImpl;

import com.epam.travelagency.domain.Entity;
import com.epam.travelagency.domain.UserTourId;
import com.epam.travelagency.repository.EntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for userTour entity.
 */
@Repository
public class UserTourRepository implements EntityRepository<Entity<UserTourId>,
                                                            UserTourId> {

    /**
     * Tour's id.
     */
    private static final String TOUR_ID = "tour_id";

    /**
     * User's id.
     */
    private static final String USER_ID = "user_id";

    private static final String ENTITY = "entity";
    /**
     * RowMapper for entity.
     */
    private static final RowMapper<Entity<UserTourId>> ROW_MAPPER =
            (resultSet, i) -> {
        Entity<UserTourId> entity = (Entity<UserTourId>) APPLICATION_CONTEXT.getBean(ENTITY);
        UserTourId userTourId = APPLICATION_CONTEXT.getBean(UserTourId.class);
        userTourId.setTourId(resultSet.getInt(TOUR_ID));
        userTourId.setUserId(resultSet.getInt(USER_ID));
        entity.setId(userTourId);
        return entity;
    };

    /**
     * JdbcTemplate.
     */
    private final JdbcTemplate jdbcTemplate;

    /**
     * Logger.
     */
    private static Logger logger =
            LoggerFactory.getLogger(UserRepository.class);

    /**
     * Script to insert entity in the "UserTour" table.
     */
    private static final String INSERT_USER_TOUR_ID =
            "INSERT INTO \"UserTour\" (user_id, tour_id) VALUES (?, ?)";

    /**
     * Script to delete entity from the "UserTour" table.
     */
    private static final String DELETE_USER_TOUR_ID =
            "DELETE FROM \"UserTour\" WHERE user_id = ? and tour_id = ?";

    /**
     * Script to find entity by user's id and tour's id in the "UserTour" table.
     */
    private static final String FIND_ENTITY =
            "SELECT tour_id, user_id FROM \"UserTour\" WHERE user_id= ? and tour_id = ?";

    /**
     * Script to find all entities in the "UserTour" table.
     */
    private static final String FIND_ALL_RECORDS =
            "SELECT user_id, tour_id FROM \"UserTour\"";

    /**
     * Script to find entity by id in the "UserTour" table.
     */
    private static final String FIND_TOUR_ID =
            "SELECT tour_id FROM \"UserTour\" WHERE user_id= ?";

    /**
     * Constructor.
     *
     * @param template JdbcTemplate.
     */
    @Autowired
    public UserTourRepository(final JdbcTemplate template) {
        this.jdbcTemplate = template;
    }

    /**
     * Create new row with entity in the "UserTour" table,
     * if there is no such entity.
     *
     * @param entity saved entity.
     */
    @Override
    public final UserTourId save(final Entity<UserTourId> entity) {
        Integer userId = entity.getId().getUserId();
        Integer actualTourId = jdbcTemplate.queryForObject(FIND_TOUR_ID,
                Integer.class, userId);
        if (!entity.getId().getTourId().equals(actualTourId)) {
            jdbcTemplate.update(INSERT_USER_TOUR_ID,
                    userId, entity.getId().getTourId());
            logger.debug("Insert new entity with userId = " + userId);
        }
        return entity.getId();
    }

    /**
     * Select all entities from the "UserTour" table.
     *
     * @return list of entities.
     */
    @Override
    public final List<Entity<UserTourId>> findAll() {
        List<Entity<UserTourId>> entities = jdbcTemplate.query(
                FIND_ALL_RECORDS, ROW_MAPPER);
        logger.debug("Find all records from UserTour! " + entities);
        return entities;
    }

    /**
     * Select an entity according to the user's id ana tour's id.
     *
     * @param id entity's id.
     * @return selected entity.
     */
    @Override
    public final Entity<UserTourId> find(final UserTourId id) {
        Entity<UserTourId> entity = null;
        int count = jdbcTemplate.update(FIND_ENTITY, id.getUserId(), id.getTourId());
        if(count > 0) {
            entity = new Entity<>();
            entity.setId(id);
        }
        return entity;
    }

    /**
     * Delete entity from the "UserTour" table according to the entity's id.
     *
     * @param id entity's id.
     */
    @Override
    public final int delete(final UserTourId id) {
        Integer userId = id.getUserId();
        Integer tourId = id.getTourId();
        int count = jdbcTemplate.update(DELETE_USER_TOUR_ID, userId, tourId);
        logger.debug("Delete userTour with userId = " + userId
               + "and tourId = " + tourId);
        return count;
    }
}
