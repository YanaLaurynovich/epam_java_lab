package com.epam.travelagency.repository.repositoryImpl;

import com.epam.travelagency.domain.Review;
import com.epam.travelagency.repository.EntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

/**
 * Repository for review entity.
 */
@Repository
public class ReviewRepository implements EntityRepository<Review, Integer> {

    /**
     * Review's id.
     */
    private static final String ID = "id";

    /**
     * Review's text.
     */
    private static final String TEXT = "text";

    /**
     * Review's date.
     */
    private static final String DATE = "date";

    /**
     * Tour's id.
     */
    private static final String TOUR_ID = "tour_id";

    /**
     * User's id.
     */
    private static final String USER_ID = "user_id";

    /**
     * RowMapper for review.
     */
    private static final RowMapper<Review> ROW_MAPPER = (resultSet, i) -> {
        Review review = APPLICATION_CONTEXT.getBean(Review.class);
        review.setId(resultSet.getInt(ID));
        review.setDate(resultSet.getDate(DATE));
        review.setText(resultSet.getString(TEXT));
        review.setTourId(resultSet.getInt(TOUR_ID));
        review.setUserId(resultSet.getInt(USER_ID));
        return review;
    };

    /**
     * JdbcTemplate.
     */
    private final JdbcTemplate jdbcTemplate;

    /**
     * Logger.
     */
    private static Logger logger =
            LoggerFactory.getLogger(ReviewRepository.class);

    /**
     * Script to insert review in the "Review" table.
     */
    private static final String INSERT_REVIEW = "INSERT INTO \"Review\" "
            + "(text, date, tour_id, user_id) VALUES (?, ?, ?, ?) RETURNING id";

    /**
     * Script to update review in the "Review" table.
     */
    private static final String UPDATE_REVIEW = "UPDATE \"Review\" SET "
            + "text = ?, date = ?, tour_id = ?, user_id = ? WHERE id = ?";

    /**
     * Script to delete review from the "Review" table.
     */
    private static final String DELETE_REVIEW =
            "DELETE FROM \"Review\" WHERE id = ?";

    /**
     * Script to find all reviews in the "Review" table.
     */
    private static final String FIND_ALL_REVIEWS =
            "SELECT id, text, date, tour_id, user_id FROM \"Review\"";

    /**
     * Script to find review by id in the "Review" table.
     */
    private static final String FIND_REVIEW =
            "SELECT id, text, date, tour_id, user_id "
                    + "FROM \"Review\" WHERE id= ?";

    /**
     * Constructor.
     *
     * @param template JdbcTemplate.
     */
    @Autowired
    public ReviewRepository(final JdbcTemplate template) {
        this.jdbcTemplate = template;
    }

    /**
     * Create new row with review in the "Review" table,
     * if there is no such review.
     * Else update existing review.
     *
     * @param review saved review.
     */
    @Override
    public final Integer save(final Review review) {
        Integer id;
        String text = review.getText();
        Date date = review.getDate();
        int tourId = review.getTourId();
        int userId = review.getUserId();
        if (review.getId() == null) {
            id = jdbcTemplate.queryForObject(INSERT_REVIEW, Integer.class,
                    text, date, tourId, userId);
            logger.debug("Insert new review with text = " + text);
        } else {
            id = review.getId();
            jdbcTemplate.update(UPDATE_REVIEW, text, date, tourId, userId, id);
            logger.debug("Update review with id = " + id);
        }
        return id;
    }

    /**
     * Select all reviews from the "Review" table.
     *
     * @return list of reviews.
     */
    @Override
    public final List<Review> findAll() {
        List<Review> reviews = jdbcTemplate.query(FIND_ALL_REVIEWS, ROW_MAPPER);
        logger.debug("Find all reviews! " + reviews);
        return reviews;
    }

    /**
     * Select a review according to the review's id in "Review" table.
     *
     * @param id review's id.
     * @return selected review.
     */
    @Override
    public final Review find(final Integer id) {
        Review review = jdbcTemplate.queryForObject(
                FIND_REVIEW, new Object[]{id}, ROW_MAPPER);
        logger.debug("Find a review. " + review);
        return review;
    }

    /**
     * Delete review from the "Review" table according to the review's id.
     *
     * @param id review's id.
     * @return id saved review.
     */
    @Override
    public final int delete(final Integer id) {
        int count = jdbcTemplate.update(DELETE_REVIEW, id);
        logger.debug("Delete review with id = " + id);
        return count;
    }
}
