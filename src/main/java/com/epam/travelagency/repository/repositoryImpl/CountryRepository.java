package com.epam.travelagency.repository.repositoryImpl;

import com.epam.travelagency.domain.Country;
import com.epam.travelagency.repository.EntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for country.
 */
@Repository
public class CountryRepository implements EntityRepository<Country, Integer> {

    /**
     * Country's id.
     */
    private static final String ID = "id";

    /**
     * Country's name.
     */
    private static final String NAME = "name";

    /**
     * RowMapper for country.
     */
    private static final RowMapper<Country> ROW_MAPPER = (resultSet, i) -> {
        Country country = APPLICATION_CONTEXT.getBean(Country.class);
        country.setId(resultSet.getInt(ID));
        country.setName(resultSet.getString(NAME));
        return country;
    };

    /**
     * JdbcTemplate.
     */
    private final JdbcTemplate jdbcTemplate;

    /**
     * Logger.
     */
    private static Logger logger =
            LoggerFactory.getLogger(CountryRepository.class);

    /**
     * Script to insert country in the "Country" table.
     */
    private static final String INSERT_COUNTRY =
            "INSERT INTO \"Country\" (name) VALUES (?) RETURNING id";

    /**
     * Script to update country in the "Country" table.
     */
    private static final String UPDATE_COUNTRY =
            "UPDATE \"Country\" SET name = ? WHERE id = ?";

    /**
     * Script to delete country from the "Country" table.
     */
    private static final String DELETE_COUNTRY =
            "DELETE FROM \"Country\" WHERE id = ?";

    /**
     * Script to find all countries in the "Country" table.
     */
    private static final String FIND_ALL_COUNTRIES =
            "SELECT id, name FROM \"Country\"";

    /**
     * Script to find country by id in the "Country" table.
     */
    private static final String FIND_COUNTRY =
            "SELECT id, name FROM \"Country\" WHERE id= ?";

    /**
     * Constructor.
     *
     * @param template JdbcTemplate.
     */
    @Autowired
    public CountryRepository(final JdbcTemplate template) {
        this.jdbcTemplate = template;
    }

    /**
     * Create new row with country in the "Country" table,
     * if there is no such country.
     * Else update existing country.
     *
     * @param country saved country.
     * @return id saved country.
     */
    @Override
    public final Integer save(final Country country) {
        Integer id;
        String name = country.getName();
        if (country.getId() == null) {
            id = jdbcTemplate.queryForObject(INSERT_COUNTRY,
                    Integer.class, name);
            logger.debug("Insert new country with name = " + name);
        } else {
            id = country.getId();
            jdbcTemplate.update(UPDATE_COUNTRY, name, id);
            logger.debug("Update user with id = " + id);
        }
        return id;
    }

    /**
     * Select all countries from the "Country" table.
     *
     * @return list of countries.
     */
    @Override
    public final List<Country> findAll() {
        List<Country> countries =
                jdbcTemplate.query(FIND_ALL_COUNTRIES, ROW_MAPPER);
        logger.debug("Find all countries! " + countries);
        return countries;
    }

    /**
     * Select a country according to the country's id in "Country" table.
     *
     * @param id country's id.
     * @return selected country.
     */
    @Override
    public final Country find(final Integer id) {
        Country country = jdbcTemplate.queryForObject(
                FIND_COUNTRY, new Object[]{id}, ROW_MAPPER);
        logger.debug("Find a country. " + country);
        return country;
    }

    /**
     * Delete country from the "Country" table according to the country's id.
     *
     * @param id country's id.
     */
    @Override
    public final int delete(final Integer id) {
        int count = jdbcTemplate.update(DELETE_COUNTRY, id);
        logger.debug("Delete country with id = " + id);
        return count;
    }
}
