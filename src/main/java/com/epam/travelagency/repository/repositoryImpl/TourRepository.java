package com.epam.travelagency.repository.repositoryImpl;

import com.epam.travelagency.domain.Tour;
import com.epam.travelagency.domain.TourType;
import com.epam.travelagency.repository.EntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

/**
 * Repository for tour entity.
 */
@Repository
public class TourRepository implements EntityRepository<Tour, Integer> {

    /**
     * Tour's id.
     */
    private static final String ID = "id";

    /**
     * Tour's photo.
     */
    private static final String PHOTO = "photo";

    /**
     * Tour's date.
     */
    private static final String DATE = "date";

    /**
     * Tour's duration.
     */
    private static final String DURATION = "duration";

    /**
     * Tour's description.
     */
    private static final String DESCRIPTION = "description";

    /**
     * Tour's cost.
     */
    private static final String COST = "cost";

    /**
     * Hotel's id.
     */
    private static final String HOTEL_ID = "hotel_id";

    /**
     * Country's id.
     */
    private static final String COUNTRY_ID = "country_id";

    /**
     * Tour's type.
     */
    private static final String TOUR_TYPE = "tour_type";

    /**
     * RowMapper for tour.
     */
    private static final RowMapper<Tour> ROW_MAPPER = (resultSet, i) -> {
        Tour tour = APPLICATION_CONTEXT.getBean(Tour.class);
        tour.setId(resultSet.getInt(ID));
        tour.setPhoto(resultSet.getString(PHOTO));
        tour.setDate(resultSet.getDate(DATE));
        tour.setDuration(resultSet.getInt(DURATION));
        tour.setDescription(resultSet.getString(DESCRIPTION));
        tour.setCost(resultSet.getDouble(COST));
        tour.setHotelId(resultSet.getInt(HOTEL_ID));
        tour.setCountryId(resultSet.getInt(COUNTRY_ID));
        tour.setType(TourType.valueOf(resultSet.getString(TOUR_TYPE)));
        return tour;
    };

    /**
     * JdbcTemplate.
     */
    private final JdbcTemplate jdbcTemplate;

    /**
     * Logger.
     */
    private static Logger logger =
            LoggerFactory.getLogger(TourRepository.class);

    /**
     * Script to insert tour in the "Tour" table.
     */
    private static final String INSERT_TOUR =
            "INSERT INTO \"Tour\" (photo, date, duration, description, cost, "
                    + "hotel_id, country_id, tour_type) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?::tourtype) RETURNING id";

    /**
     * Script to update tour in the "Tour" table.
     */
    private static final String UPDATE_TOUR =
            "UPDATE \"Tour\" SET photo = ?, date = ?, duration = ?, "
                    + "description = ?, cost = ?, hotel_id = ?, "
                    + "country_id = ?, tour_type = ?::tourtype WHERE id = ?";

    /**
     * Script to delete tour from the "Tour" table.
     */
    private static final String DELETE_TOUR =
            "DELETE FROM \"Tour\" WHERE id = ?";

    /**
     * Script to find all tours in the "Tour" table.
     */
    private static final String FIND_ALL_TOURS =
            "SELECT id, photo, date, duration, description, cost, hotel_id, "
                    + "country_id, tour_type FROM \"Tour\"";

    /**
     * Script to find tour by id in the "Tour" table.
     */
    private static final String FIND_TOUR =
            "SELECT id, photo, date, duration, description, cost, hotel_id, "
                    + "country_id, tour_type FROM \"Tour\" WHERE id= ?";

    /**
     * Constructor.
     *
     * @param template JdbcTemplate.
     */
    @Autowired
    public TourRepository(final JdbcTemplate template) {
        this.jdbcTemplate = template;
    }

    /**
     * Create new row with tour in the "Tour" table, if there is no such tour.
     * Else update existing tour.
     *
     * @param tour saved tour.
     * @return id saved tour.
     */
    @Override
    public final Integer save(final Tour tour) {
        Integer id;
        String photo = tour.getPhoto();
        Date date = tour.getDate();
        int duration = tour.getDuration();
        String description = tour.getDescription();
        double cost = tour.getCost();
        long hotelId = tour.getHotelId();
        long countryId = tour.getCountryId();
        String tourType = tour.getType().name();
        if (tour.getId() == null) {
            id = jdbcTemplate.queryForObject(INSERT_TOUR, Integer.class, photo,
                    date, duration, description, cost, hotelId, countryId,
                    tourType);
            logger.debug("Insert new tour with photo = " + photo);
        } else {
            id = tour.getId();
           jdbcTemplate.update(UPDATE_TOUR, photo, date, duration, description,
                    cost, hotelId, countryId, tourType, id);
            logger.debug("Update tour with id = " + id);
        }
        return id;
    }

    /**
     * Select all tours from the "Tour" table.
     *
     * @return list of tours.
     */
    @Override
    public final List<Tour> findAll() {
        List<Tour> tours = jdbcTemplate.query(FIND_ALL_TOURS, ROW_MAPPER);
        logger.debug("Find all tours! " + tours);
        return tours;
    }

    /**
     * Select a tour according to the tour's id in "Tour" table.
     *
     * @param id tour's id.
     * @return selected tour.
     */
    @Override
    public final Tour find(final Integer id) {
        Tour tour = jdbcTemplate.queryForObject(
                FIND_TOUR, new Object[]{id}, ROW_MAPPER);
        logger.debug("Find a tour. " + tour);
        return tour;
    }

    /**
     * Delete tour from the "Tour" table according to the tour's id.
     *
     * @param id tour's id.
     */
    @Override
    public final int delete(final Integer id) {
        int count = jdbcTemplate.update(DELETE_TOUR, id);
        logger.debug("Delete tour with id = " + id);
        return count;
    }
}
