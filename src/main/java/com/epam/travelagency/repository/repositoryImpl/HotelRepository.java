package com.epam.travelagency.repository.repositoryImpl;

import com.epam.travelagency.domain.Feature;
import com.epam.travelagency.domain.Hotel;
import com.epam.travelagency.repository.EntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Repository for hotel entity.
 */
@Repository
public class HotelRepository implements EntityRepository<Hotel, Integer> {
    /**
     * Hotel's id.
     */
    private static final String ID = "id";

    /**
     * Hotel's name.
     */
    private static final String NAME = "name";

    /**
     * Hotel's stars.
     */
    private static final String STARS = "stars";

    /**
     * Hotel's website.
     */
    private static final String WEBSITE = "website";

    /**
     * Hotel's latitude.
     */
    private static final String LATITUDE = "latitude";

    /**
     * Hotel's longitude.
     */
    private static final String LONGITUDE = "longitude";

    /**
     * Hotel's features.
     */
    private static final String FEATURES = "features";

    /**
     * Type of column with features.
     */
    private static final String FEATURE = "feature";

    /**
     * RowMapper for hotel.
     */
    private static final RowMapper<Hotel> ROW_MAPPER = (resultSet, i) -> {
        Hotel hotel = APPLICATION_CONTEXT.getBean(Hotel.class);
        hotel.setId(resultSet.getInt(ID));
        hotel.setName(resultSet.getString(NAME));
        hotel.setStars(resultSet.getInt(STARS));
        hotel.setWebsite(resultSet.getString(WEBSITE));
        hotel.setLatitude(resultSet.getFloat(LATITUDE));
        hotel.setLongitude(resultSet.getFloat(LONGITUDE));

        List<Feature> features = new ArrayList<>();
        Array featuresArray = resultSet.getArray(FEATURES);
        Object[] array = (Object[]) featuresArray.getArray();
        for (Object obj : array) {
            features.add(Feature.valueOf(obj.toString()));
        }
        hotel.setFeatures(features);
        return hotel;
    };

    /**
     * JdbcTemplate.
     */
    private final JdbcTemplate jdbcTemplate;

    /**
     * Logger.
     */
    private static Logger logger =
            LoggerFactory.getLogger(HotelRepository.class);

    /**
     * Script to insert hotel in the "Hotel" table.
     */
    private static final String INSERT_HOTEL = "INSERT INTO \"Hotel\" "
            + "(name, stars, website, latitude, longitude, features) "
            + "VALUES (?, ?, ?, ?, ?, ?) RETURNING id";

    /**
     * Script to update hotel in the "Hotel" table.
     */
    private static final String UPDATE_HOTEL = "UPDATE \"Hotel\" SET "
            + "name = ?, stars = ?, website = ?, latitude = ?, longitude = ?, "
            + "features = ?::feature[] WHERE id = ?";

    /**
     * Script to delete hotel from the "Hotel" table.
     */
    private static final String DELETE_HOTEL =
            "DELETE FROM \"Hotel\" WHERE id = ?";

    /**
     * Script to find all hotels in the "Hotel" table.
     */
    private static final String FIND_ALL_HOTELS =
            "SELECT id, name, stars, website, latitude, longitude, features "
                    + "FROM \"Hotel\"";

    /**
     * Script to find hotel by id in the "Hotel" table.
     */
    private static final String FIND_HOTEL =
            "SELECT id, name, stars, website, latitude, longitude, features "
                    + "FROM \"Hotel\" WHERE id= ?";

    /**
     * Constructor.
     *
     * @param template JdbcTemplate.
     */
    @Autowired
    public HotelRepository(final JdbcTemplate template) {
        this.jdbcTemplate = template;
    }

    /**
     * Create new row with hotel in the "Hotel" table,
     * if there is no such hotel.
     * Else update existing hotel.
     *
     * @param hotel saved hotel.
     * @return id saved hotel.
     */
    @Override
    public final Integer save(final Hotel hotel) {
        Integer id;
        String name = hotel.getName();
        int stars = hotel.getStars();
        String website = hotel.getWebsite();
        double latitude = hotel.getLatitude();
        double longitude = hotel.getLongitude();
        List<Feature> features = hotel.getFeatures();
        Array featureArray = prepareSqlArray(features);
        if (hotel.getId() == null) {
            id = jdbcTemplate.queryForObject(INSERT_HOTEL, Integer.class, name,
                    stars, website, latitude, longitude, featureArray);
            logger.debug("Insert new hotel with name = " + name);
        } else {
            id = hotel.getId();
            jdbcTemplate.update(UPDATE_HOTEL, name, stars, website, latitude,
                    longitude, featureArray, id);
            logger.debug("Update hotel with id = " + id);
        }
        return id;
    }

    /**
     * Select all hotels from the "Hotel" table.
     *
     * @return list of hotels.
     */
    @Override
    public final List<Hotel> findAll() {
        List<Hotel> hotels = jdbcTemplate.query(FIND_ALL_HOTELS, ROW_MAPPER);
        logger.debug("Find all hotels! " + hotels);
        return hotels;
    }

    /**
     * Select a hotel according to the hotel's id in "Hotel" table.
     *
     * @param id hotel's id.
     * @return selected hotel.
     */
    @Override
    public final Hotel find(final Integer id) {
        Hotel hotel = jdbcTemplate.queryForObject(
                FIND_HOTEL, new Object[]{id}, ROW_MAPPER);
        logger.debug("Find a hotel. " + hotel);
        return hotel;
    }

    /**
     * Delete hotel from the "Hotel" table according to the hotel's id.
     *
     * @param id hotel's id.
     */
    @Override
    public final int delete(final Integer id) {
        int count = jdbcTemplate.update(DELETE_HOTEL, id);
        logger.debug("Delete hotel with id = " + id);
        return count;
    }

    /**
     * Change list of features to sqlArray.
     *
     * @param features list of features.
     * @return sqlArray.
     */
    private Array prepareSqlArray(final List<Feature> features) {
        List<String> sqlFeatures = new ArrayList<>();
        for (Feature feature : features) {
            String featureName = feature.name();
            sqlFeatures.add(featureName);
        }
        try {
            return Objects.requireNonNull(jdbcTemplate.getDataSource())
                    .getConnection().createArrayOf(FEATURE,
                            sqlFeatures.toArray());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
