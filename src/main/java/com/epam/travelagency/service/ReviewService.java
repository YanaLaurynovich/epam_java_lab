package com.epam.travelagency.service;

import com.epam.travelagency.domain.Review;
import com.epam.travelagency.repository.EntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 * Service for ReviewRepository.
 */
@Service
public class ReviewService {

    /**
     * ReviewRepository.
     */
    private EntityRepository<Review, Integer> reviewRepository;

    /**
     * Review.
     */
    private Review review;

    /**
     * Constructor.
     * @param reviewRepository reviewRepository.
     * @param review review.
     */
    @Autowired
    public ReviewService(EntityRepository<Review, Integer> reviewRepository, Review review) {
        this.reviewRepository = reviewRepository;
        this.review = review;
    }

    /**
     * Valid params and save review in db.
     * @param text review's text.
     * @param date review's date.
     * @param tourId tour's id.
     * @param userId user's id.
     * @return review's id.
     */
    public final Integer save(final String text, final LocalDate date,
                              final int tourId, final int userId) {
        review.setText(text);
        review.setDate(java.sql.Date.valueOf(date));
        review.setTourId(tourId);
        review.setUserId(userId);
        Integer id = reviewRepository.save(review);
        return id;
    }

    /**
     * Valid params and update review in db.
     * @param id review's id.
     * @param text review's text.
     * @param date review's date.
     * @param tourId tour's id.
     * @param userId user's id.
     * @return review's id.
     */
    public final Integer update(final Integer id, final String text,
                                final LocalDate date, final int tourId,
                                final int userId) {
        review.setId(id);
        review.setTourId(tourId);
        review.setUserId(userId);
        review.setText(text);
        review.setDate(java.sql.Date.valueOf(date));
        Integer updatedId = reviewRepository.save(review);
        return updatedId;
    }

    /**
     * Valid params and find review in db.
     * @param id review's id.
     * @return review.
     */
    public final Review find(final Integer id) {
        return reviewRepository.find(id);
    }

    /**
     * FInd all reviews.
     * @return list of reviews.
     */
    public final List<Review> findAll() {
        return reviewRepository.findAll();
    }

    /**
     * Valid params and delete review from db.
     * @param id review's id.
     * @return count of deleted reviews.
     */
    public final int delete(final Integer id) {
        int count = reviewRepository.delete(id);
        return count;
    }
}
