package com.epam.travelagency.service;

import com.epam.travelagency.domain.User;
import com.epam.travelagency.repository.EntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for UserRepository.
 */
@Service
public class UserService {

    /**
     * UserRepository.
     */
    private EntityRepository<User, Integer> userRepository;

    /**
     * User.
     */
    private User user;

    /**
     * Constructor.
     * @param userRepository userRepository.
     * @param user user.
     */
    @Autowired
    public UserService(EntityRepository<User, Integer> userRepository,
                       User user) {
        this.userRepository = userRepository;
        this.user = user;
    }

    /**
     * Valid params and save user in db.
     * @param login user's login.
     * @param password user's password.
     * @return user's id.
     */
    public final Integer save(final String login, final String password) {
        user.setLogin(login);
        user.setPassword(password);
        Integer id = userRepository.save(user);
        return id;
    }

    /**
     * Valid params and update user in db.
     * @param id user's id.
     * @param login user's login.
     * @param password user's password.
     * @return user's id.
     */
    public final Integer update(final Integer id, final String login,
                                final String password) {
        user.setId(id);
        user.setLogin(login);
        user.setPassword(password);
        Integer updatedId = userRepository.save(user);
        return updatedId;
    }

    /**
     * Valid params and find user in db.
     * @param id user's id.
     * @return user.
     */
    public final User find(final Integer id) {
        return userRepository.find(id);
    }

    /**
     * FInd all users.
     * @return list of users.
     */
    public final List<User> findAll() {
        return userRepository.findAll();
    }

    /**
     * Valid params and delete user from db.
     * @param id user's id.
     * @return count of deleted users.
     */
    public final int delete(final Integer id) {
        return userRepository.delete(id);
    }

}
