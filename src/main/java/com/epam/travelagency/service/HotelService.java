package com.epam.travelagency.service;

import com.epam.travelagency.domain.Feature;
import com.epam.travelagency.domain.Hotel;
import com.epam.travelagency.repository.EntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service for HotelRepository.
 */
@Service
public class HotelService {

    /**
     * HotelRepository.
     */
    private EntityRepository<Hotel, Integer> hotelRepository;

    /**
     * Hotel.
     */
    private Hotel hotel;

    /**
     * Constructor.
     * @param hotelRepository hotelRepository.
     * @param hotel hotel.
     */
    @Autowired
    public HotelService(EntityRepository<Hotel, Integer> hotelRepository, Hotel hotel) {
        this.hotelRepository = hotelRepository;
        this.hotel = hotel;
    }

    /**
     * Valid params and save hotel in db.
     * @param name hotel's name.
     * @param stars hotel's stars.
     * @param website hotel's website.
     * @param latitude hotel's latitude.
     * @param longitude hotel's longitude.
     * @param list hotel's features.
     * @return hotel's id.
     */
    public final Integer save(final String name, final int stars,
                              final String website, final double latitude,
                              final double longitude, List<String> list) {
        hotel.setName(name);
        hotel.setStars(stars);
        hotel.setWebsite(website);
        hotel.setLatitude(latitude);
        hotel.setLongitude(longitude);
        List<Feature> features = new ArrayList<>();
        list.forEach(current -> {
            features.add(Feature.valueOf(current));
        });
        hotel.setFeatures(features);
        Integer id = hotelRepository.save(hotel);
        return id;
    }

    /**
     * Valid params and save hotel in db.
     * @param id hotel's id.
     * @param name hotel's name.
     * @param stars hotel's stars.
     * @param website hotel's website.
     * @param latitude hotel's latitude.
     * @param longitude hotel's longitude.
     * @param list hotel's features.
     * @return hotel's id.
     */
    public final Integer update(final Integer id, final String name, final int stars,
                                final String website, final double latitude,
                                final double longitude, List<String> list) {
        hotel.setId(id);
        hotel.setStars(stars);
        hotel.setName(name);
        hotel.setLatitude(latitude);
        hotel.setWebsite(website);
        List<Feature> features = new ArrayList<>();
        list.forEach(current -> {
            features.add(Feature.valueOf(current));
        });
        hotel.setFeatures(features);
        hotel.setLongitude(longitude);
        Integer updatedId = hotelRepository.save(hotel);
        return updatedId;
    }

    /**
     * Valid params and find hotel in db.
     * @param id hotel's id.
     * @return hotel.
     */
    public final Hotel find(final Integer id) {
        return hotelRepository.find(id);
    }

    /**
     * FInd all hotels.
     * @return list of hotels.
     */
    public final List<Hotel> findAll() {
        return hotelRepository.findAll();
    }

    /**
     * Valid params and delete hotel from db.
     * @param id hotel's id.
     * @return count of deleted hotels.
     */
    public final int delete(final Integer id) {
        return hotelRepository.delete(id);
    }
}
