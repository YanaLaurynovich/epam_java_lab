package com.epam.travelagency.service;

import com.epam.travelagency.domain.Country;
import com.epam.travelagency.repository.EntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for CountryRepository.
 */
@Service
public class CountryService {

    /**
     * CountryRepository.
     */
    private EntityRepository<Country, Integer> countryRepository;

    /**
     * Country.
     */
    private Country country;

    /**
     * Constructor.
     * @param countryRepository countryRepository.
     * @param country country.
     */
    @Autowired
    public CountryService(EntityRepository<Country, Integer> countryRepository, Country country) {
        this.countryRepository = countryRepository;
        this.country = country;
    }

    /**
     * Valid params and save country in db.
     * @param name country's name.
     * @return country's id.
     */
    public final Integer save(final String name) {
        country.setName(name);
        Integer id = countryRepository.save(country);
        return id;
    }

    /**
     * Valid params and update country in db.
     * @param id country's id.
     * @param name country's name.
     * @return country's id.
     */
    public final Integer update(final Integer id, final String name) {
        country.setId(id);
        country.setName(name);
        Integer updatedId = countryRepository.save(country);
        return updatedId;
    }

    /**
     * Valid params and find country in db.
     * @param id country's id.
     * @return country.
     */
    public final Country find(final Integer id) {
        return countryRepository.find(id);
    }

    /**
     * FInd all countries.
     * @return list of countries.
     */
    public final List<Country> findAll() {
        return countryRepository.findAll();
    }

    /**
     * Valid params and delete country from db.
     * @param id country's id.
     * @return count of deleted countries.
     */
    public final int delete(final Integer id) {
        return countryRepository.delete(id);
    }

}
