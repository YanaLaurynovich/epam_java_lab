package com.epam.travelagency.service;

import com.epam.travelagency.domain.Tour;
import com.epam.travelagency.domain.TourType;
import com.epam.travelagency.repository.EntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

/**
 * Service for TourRepository.
 */
@Service
public class TourService {

    /**
     * TourRepository.
     */
    private EntityRepository<Tour, Integer> tourRepository;

    /**
     * Tour.
     */
    private Tour tour;

    /**
     * Constructor
     * @param tourRepository tourRepository.
     * @param tour tour.
     */
    @Autowired
    public TourService(EntityRepository<Tour, Integer> tourRepository, Tour tour) {
        this.tourRepository = tourRepository;
        this.tour = tour;
    }

    /**
     * Valid params and save tour in db.
     * @param photo tour's photo.
     * @param date tour's date.
     * @param duration tour's duration.
     * @param description tour's description.
     * @param cost tour's cost.
     * @param hotelId hotel's id.
     * @param countryId country's id.
     * @param type tour's type.
     * @return tour's id.
     */
    public final Integer save(final String photo, final LocalDate date,
                              final int duration, final String description,
                              final double cost, final int hotelId,
                              final int countryId, final String type) {
        tour.setPhoto(photo);
        tour.setDate(Date.valueOf(date));
        tour.setDuration(duration);
        tour.setDescription(description);
        tour.setCost(cost);
        tour.setHotelId(hotelId);
        tour.setCountryId(countryId);
        tour.setType(TourType.valueOf(type));
        Integer id = tourRepository.save(tour);
        return id;
    }

    /**
     * Valid params and save tour in db.
     * @param id tour's id.
     * @param photo tour's photo.
     * @param date tour's date.
     * @param duration tour's duration.
     * @param description tour's description.
     * @param cost tour's cost.
     * @param hotelId hotel's id.
     * @param countryId country's id.
     * @param type tour's type.
     * @return tour's id.
     */
    public final Integer update(final Integer id, final String photo, final LocalDate date,
                                final int duration, final String description,
                                final double cost, final int hotelId,
                                final int countryId, final String type) {
        tour.setId(id);
        tour.setDate(Date.valueOf(date));
        tour.setPhoto(photo);
        tour.setDuration(duration);
        tour.setCost(cost);
        tour.setDescription(description);
        tour.setCountryId(countryId);
        tour.setHotelId(hotelId);
        tour.setType(TourType.valueOf(type));
        Integer updatedId = tourRepository.save(tour);
        return updatedId;
    }

    /**
     * Valid params and find hotel in db.
     * @param id hotel's id.
     * @return hotel.
     */
    public final Tour find(final Integer id) {
        return tourRepository.find(id);
    }

    /**
     * FInd all hotels.
     * @return list of hotels.
     */
    public final List<Tour> findAll() {
        return tourRepository.findAll();
    }

    /**
     * Valid params and delete hotel from db.
     * @param id hotel's id.
     * @return count of deleted hotels.
     */
    public final int delete(final Integer id) {
        return tourRepository.delete(id);
    }
}
