package com.epam.travelagency.service;

import com.epam.travelagency.domain.Entity;
import com.epam.travelagency.domain.UserTourId;
import com.epam.travelagency.repository.EntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for UserTourRepository.
 */
@Service
public class UserTourService {

    /**
     * UserTourRepository.
     */
    private EntityRepository<Entity<UserTourId>, UserTourId> userTourRepository;

    /**
     * userTour entity.
     */
    private Entity<UserTourId> userTour;

    /**
     * Constructor.
     * @param userTourRepository userTourRepository.
     * @param userTour userTour.
     */
    @Autowired
    public UserTourService(EntityRepository<Entity<UserTourId>, UserTourId> userTourRepository, Entity<UserTourId> userTour) {
        this.userTourRepository = userTourRepository;
        this.userTour = userTour;
    }

    /**
     * Valid params and save userTour entity in db.
     * @param userId user's id.
     * @param tourId tour's id.
     * @return userTour's id.
     */
    public final UserTourId save(final int userId, final int tourId) {
        UserTourId utid = new UserTourId();
        utid.setUserId(userId);
        utid.setTourId(tourId);
        userTour.setId(utid);
        UserTourId id = userTourRepository.save(userTour);
        return id;
    }

    /**
     * Valid params and find userTour entity in db.
     * @param userId user's id.
     * @param tourId tour's id.
     * @return userTour's id.
     */
    public final Entity<UserTourId> find(final int userId, final int tourId) {
        UserTourId utid = new UserTourId();
        utid.setUserId(userId);
        utid.setTourId(tourId);
        return userTourRepository.find(utid);
    }

    /**
     * FInd all userTour entities.
     * @return list of entities.
     */
    public final List<Entity<UserTourId>> findAll() {
        return userTourRepository.findAll();
    }

    /**
     * Valid params and delete userTour entity from db.
     * @param userId user's id.
     * @param tourId tour's id.
     * @return count of deleted entities.
     */
    public final int delete(final int userId, final int tourId) {
        UserTourId utid = new UserTourId();
        utid.setUserId(userId);
        utid.setTourId(tourId);
        return userTourRepository.delete(utid);
    }
}
