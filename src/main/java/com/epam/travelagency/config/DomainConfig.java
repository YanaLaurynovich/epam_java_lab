package com.epam.travelagency.config;

import com.epam.travelagency.domain.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Config file for domain entities.
 */
@Configuration
public class DomainConfig {

    /**
     * Simple entity.
     * @return entity.
     */
    @Bean(name = "entity")
    public Entity entity() {
        return new Entity();
    }

    /**
     * User entity.
     * @return user.
     */
    @Bean
    public User user() {
        return new User();
    }

    /**
     * Country entity.
     * @return country.
     */
    @Bean
    public Country country() {
        return new Country();
    }

    /**
     * Hotel entity.
     * @return hotel.
     */
    @Bean
    public Hotel hotel() {
        return new Hotel();
    }

    /**
     * Review entity.
     * @return review.
     */
    @Bean
    public Review review() {
        return new Review();
    }

    /**
     * Tour entity.
     * @return tour.
     */
    @Bean
    public Tour tour() {
        return new Tour();
    }

    /**
     * UserTourId.
     * @return UserTourId.
     */
    @Bean
    public UserTourId userTourId() {
        return new UserTourId();
    }
}
