package com.epam.travelagency.config;

import com.epam.travelagency.domain.*;
import com.epam.travelagency.repository.EntityRepository;
import com.epam.travelagency.repository.repositoryImpl.CountryRepository;
import com.epam.travelagency.repository.repositoryImpl.HotelRepository;
import com.epam.travelagency.repository.repositoryImpl.ReviewRepository;
import com.epam.travelagency.repository.repositoryImpl.TourRepository;
import com.epam.travelagency.repository.repositoryImpl.UserRepository;
import com.epam.travelagency.repository.repositoryImpl.UserTourRepository;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Properties;

/**
 * Config file for repositories.
 */
@Configuration
public class RepositoryConfig {

    /**
     * Create hikariConfig for  HikariCP.
     *
     * @return hikariConfig.
     */
    @Bean(name = "hikariConfig")
    public HikariConfig hikariConfig() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setPoolName("springHikariCP");
        hikariConfig.
                setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
        hikariConfig.setDataSourceProperties(dataSourceProperties());
        return hikariConfig;
    }

    /**
     * Create data source properties for hikariConfig.
     *
     * @return dataSourceProperties.
     */
    @Bean(name = "dataSourceProperties")
    public Properties dataSourceProperties() {
        Properties properties = new Properties();
        properties.setProperty("databaseName", "travel_agency");
        properties.setProperty("serverName", "127.0.0.1");
        properties.setProperty("user", "postgres");
        properties.setProperty("password", "Super_mario_01");
        return properties;
    }

    /**
     * Create hikariDatasource.
     *
     * @param hikariConfig hikari config.
     * @return HikariDataSource.
     */
    @Bean(name = "dataSource", destroyMethod = "close")
    @Autowired
    public HikariDataSource dataSource(final HikariConfig hikariConfig) {
        return new HikariDataSource(hikariConfig);
    }

    /**
     * Create jdbcTemplate.
     *
     * @param dataSource HikariDataSource.
     * @return jdbcTemplate.
     */
    @Bean(name = "jdbcTemplate")
    @Autowired
    public JdbcTemplate jdbcTemplate(final HikariDataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    /**
     * Create userRepository.
     *
     * @param jdbcTemplate JdbcTemplate.
     * @return UserRepository.
     */
    @Bean(name = "userRepository")
    @Autowired
    public EntityRepository<User, Integer> userRepository(final JdbcTemplate jdbcTemplate) {
        return new UserRepository(jdbcTemplate);
    }

    /**
     * Create countryRepository.
     *
     * @param jdbcTemplate JdbcTemplate.
     * @return CountryRepository.
     */
    @Bean(name = "countryRepository")
    @Autowired
    public EntityRepository<Country, Integer> countryRepository(final JdbcTemplate jdbcTemplate) {
        return new CountryRepository(jdbcTemplate);
    }

    /**
     * Create hotelRepository.
     *
     * @param jdbcTemplate JdbcTemplate.
     * @return HotelService.
     */
    @Bean(name = "hotelRepository")
    @Autowired
    public EntityRepository<Hotel, Integer> hotelRepository(final JdbcTemplate jdbcTemplate) {
        return new HotelRepository(jdbcTemplate);
    }

    /**
     * Create reviewRepository.
     *
     * @param jdbcTemplate JdbcTemplate.
     * @return ReviewRepository.
     */
    @Bean(name = "reviewRepository")
    @Autowired
    public EntityRepository<Review, Integer> reviewRepository(final JdbcTemplate jdbcTemplate) {
        return new ReviewRepository(jdbcTemplate);
    }

    /**
     * Create tourRepository.
     *
     * @param jdbcTemplate JdbcTemplate.
     * @return TourRepository.
     */
    @Bean(name = "tourRepository")
    @Autowired
    public EntityRepository<Tour, Integer> tourRepository(final JdbcTemplate jdbcTemplate) {
        return new TourRepository(jdbcTemplate);
    }

    /**
     * Create userTourRepository.
     * @param jdbcTemplate JdbcTemplate.
     * @return UserTourRepository.
     */
    @Bean(name = "userTourRepository")
    @Autowired
    public EntityRepository<Entity<UserTourId>, UserTourId> userTourRepository(
            final JdbcTemplate jdbcTemplate) {
        return new UserTourRepository(jdbcTemplate);
    }
}
