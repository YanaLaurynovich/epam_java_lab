
SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;


CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';

CREATE TYPE public.feature AS ENUM (
  'FREE_SMOKE',
  'WATERPARK',
  'FREE_INTERNET',
  'NO_PETS',
  'FREE_PARKING',
  'AIR_CONDITIONED',
  'CHILDREN_FRIENDLY',
  'NON_SMOKING',
  'IN_ROOM_SAFES',
  'ON_SITE_BIKE_RENTALS'
  );


CREATE TYPE public.tourtype AS ENUM (
  'BUS',
  'BIKE',
  'GASTRONOMIC',
  'CRUISES',
  'WINTER',
  'WEDDING',
  'WEEKEND',
  'SHOP',
  'EXTREME',
  'SIGHTSEEING',
  'CHILD',
  'EDUCATIONAL',
  'MEDICAL',
  'PILIGRIM',
  'EXPEDITION',
  'CITY'
);


SET default_with_oids = false;

CREATE TABLE public."Country" (
                                name text NOT NULL,
                                id bigint NOT NULL,
                                CONSTRAINT "Country_pkey" PRIMARY KEY (id)
);

CREATE SEQUENCE public."Country_id_seq"
  START WITH 1
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE
  CACHE 1;

ALTER SEQUENCE public."Country_id_seq" OWNED BY public."Country".id;

CREATE TABLE public."Hotel" (
                              name text NOT NULL,
                              stars bigint NOT NULL,
                              website text,
                              latitude real,
                              longitude real,
                              id bigint NOT NULL,
                              features public.feature[],
                              CONSTRAINT "Hotel_pkey" PRIMARY KEY (id)
);

CREATE SEQUENCE public."Hotel_id_seq"
  START WITH 1
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE
  CACHE 1;

ALTER SEQUENCE public."Hotel_id_seq" OWNED BY public."Hotel".id;

CREATE TABLE public."Review" (
                               date date,
                               text text,
                               user_id bigint NOT NULL,
                               tour_id bigint NOT NULL,
                               id bigint NOT NULL,
                               CONSTRAINT "Review_pkey" PRIMARY KEY (id)
);

CREATE SEQUENCE public."Review_id_seq"
  START WITH 1
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE
  CACHE 1;

ALTER SEQUENCE public."Review_id_seq" OWNED BY public."Review".id;

CREATE TABLE public."Tour" (
                             photo text,
                             date date NOT NULL,
                             description text,
                             cost double precision,
                             hotel_id bigint NOT NULL,
                             country_id bigint NOT NULL,
                             tour_type public.tourtype NOT NULL,
                             id bigint NOT NULL,
                             duration bigint,
                             CONSTRAINT "Tour_pkey" PRIMARY KEY (id)
);

CREATE SEQUENCE public."Tour_id_seq"
  START WITH 1
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE
  CACHE 1;

ALTER SEQUENCE public."Tour_id_seq" OWNED BY public."Tour".id;


CREATE TABLE public."User" (
                             login text NOT NULL,
                             password text NOT NULL,
                             id bigint NOT NULL,
                             CONSTRAINT "User_pkey" PRIMARY KEY (id)
);

CREATE TABLE public."UserTour" (
                                 user_id bigint NOT NULL,
                                 tour_id bigint NOT NULL
);


CREATE SEQUENCE public."User_id_seq"
  START WITH 1
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE
  CACHE 1;

ALTER SEQUENCE public."User_id_seq" OWNED BY public."User".id;

ALTER TABLE ONLY public."Country" ALTER COLUMN id SET DEFAULT nextval('public."Country_id_seq"'::regclass);

ALTER TABLE ONLY public."Hotel" ALTER COLUMN id SET DEFAULT nextval('public."Hotel_id_seq"'::regclass);

ALTER TABLE ONLY public."Review" ALTER COLUMN id SET DEFAULT nextval('public."Review_id_seq"'::regclass);

ALTER TABLE ONLY public."Tour" ALTER COLUMN id SET DEFAULT nextval('public."Tour_id_seq"'::regclass);

ALTER TABLE ONLY public."User" ALTER COLUMN id SET DEFAULT nextval('public."User_id_seq"'::regclass);

