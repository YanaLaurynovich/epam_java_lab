package com.epam.travelagency.service;

import com.epam.travelagency.domain.Entity;
import com.epam.travelagency.domain.UserTourId;
import com.epam.travelagency.repository.EntityRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserTourServiceTest {
    @Mock
    private static EntityRepository<Entity<UserTourId>, UserTourId> repository;

    @Mock
    private static Entity<UserTourId> entity;

    @InjectMocks
    private static UserTourService userTourService = new UserTourService(repository, entity);


    @Test
    public void saveTestPositive() {
        UserTourId userTourId = new UserTourId();
        userTourId.setTourId(1);
        userTourId.setUserId(1);
        when(repository.save(entity)).thenReturn(userTourId);
        UserTourId id = userTourService.save(1, 1);
        Assert.assertEquals(userTourId, id);
    }

    @Test
    public void findTestPositive() {
        when(repository.find(any(UserTourId.class))).thenReturn(entity);
        Entity<UserTourId> actual = userTourService.find(1, 1);
        Assert.assertNotNull(actual);
    }

    @Test
    public void findAllTestPositive() {
        List<Entity<UserTourId>> entityList = new ArrayList<>();
        when(repository.findAll()).thenReturn(entityList);
        List<Entity<UserTourId>> actual = userTourService.findAll();
        Assert.assertNotNull(actual);
    }

    @Test
    public void deleteTestPositive() {
        when(repository.delete(any(UserTourId.class))).thenReturn(1);
        int count = userTourService.delete(1, 1);
        Assert.assertEquals(1, count);
    }

}
