package com.epam.travelagency.service;

import com.epam.travelagency.domain.User;
import com.epam.travelagency.repository.EntityRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private static EntityRepository<User, Integer> repository;

    @Mock
    private static User user;

    @InjectMocks
    private static UserService userService = new UserService(repository, user);


    @Test
    public void saveTestPositive() {
        when(repository.save(user)).thenReturn(1);
        Integer id = userService.save("adjsd", "saa");
        Assert.assertEquals(Integer.valueOf(1), id);
    }

    @Test
    public void updateTestPositive() {
        when(repository.save(user)).thenReturn(1);
        Integer id = userService.update(1, "adjsd", "saa");
        Assert.assertEquals(Integer.valueOf(1), id);
    }

    @Test
    public void findTestPositive() {
        when(repository.find(anyInt())).thenReturn(user);
        User actual = userService.find(1);
        Assert.assertNotNull(actual);
    }

    @Test
    public void findAllTestPositive() {
        List<User> userList = new ArrayList<>();
        when(repository.findAll()).thenReturn(userList);
        List<User> actual = userService.findAll();
        Assert.assertNotNull(actual);
    }

    @Test
    public void deleteTestPositive() {
        when(repository.delete(anyInt())).thenReturn(1);
        int count = userService.delete(1);
        Assert.assertEquals(1, count);
    }

}
