package com.epam.travelagency.service;

import com.epam.travelagency.domain.Country;
import com.epam.travelagency.repository.EntityRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceTest {
    @Mock
    private static EntityRepository<Country, Integer> repository;

    @Mock
    private static Country country;

    @InjectMocks
    private static CountryService countryService = new CountryService(repository, country);


    @Test
    public void saveTestPositive() {
        when(repository.save(country)).thenReturn(1);
        Integer id = countryService.save("Belarus");
        Assert.assertEquals(Integer.valueOf(1), id);
    }

    @Test
    public void updateTestPositive() {
        when(repository.save(country)).thenReturn(1);
        Integer id = countryService.update(1, "Belarus");
        Assert.assertEquals(Integer.valueOf(1), id);
    }

    @Test
    public void findTestPositive() {
        when(repository.find(anyInt())).thenReturn(country);
        Country actual = countryService.find(1);
        Assert.assertNotNull(actual);
    }

    @Test
    public void findAllTestPositive() {
        List<Country> countryList = new ArrayList<>();
        when(repository.findAll()).thenReturn(countryList);
        List<Country> actual = countryService.findAll();
        Assert.assertNotNull(actual);
    }

    @Test
    public void deleteTestPositive() {
        when(repository.delete(anyInt())).thenReturn(1);
        int count = countryService.delete(1);
        Assert.assertEquals(1, count);
    }
}
