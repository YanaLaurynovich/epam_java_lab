package com.epam.travelagency.service;

import com.epam.travelagency.domain.Hotel;
import com.epam.travelagency.repository.EntityRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HotelServiceTest {
    @Mock
    private static EntityRepository<Hotel, Integer> repository;

    @Mock
    private static Hotel hotel;

    @InjectMocks
    private static HotelService hotelService = new HotelService(repository, hotel);


    @Test
    public void saveTestPositive() {
        when(repository.save(hotel)).thenReturn(1);
        List<String> listFeatures = new ArrayList<>();
        listFeatures.add("FREE_SMOKE");
        Integer id = hotelService.save("Belarus", 3, "web@df.vv",
                -23.5, 65.4, listFeatures);
        Assert.assertEquals(Integer.valueOf(1), id);
    }

    @Test
    public void updateTestPositive() {
        when(repository.save(hotel)).thenReturn(1);
        List<String> listFeatures = new ArrayList<>();
        listFeatures.add("FREE_SMOKE");
        Integer id = hotelService.update(1, "Belarus", 3, "web@df.vv",
                -23.5, 65.4, listFeatures);
        Assert.assertEquals(Integer.valueOf(1), id);
    }

    @Test
    public void findTestPositive() {
        when(repository.find(anyInt())).thenReturn(hotel);
        Hotel actual = hotelService.find(1);
        Assert.assertNotNull(actual);
    }

    @Test
    public void findAllTestPositive() {
        List<Hotel> hotelList = new ArrayList<>();
        when(repository.findAll()).thenReturn(hotelList);
        List<Hotel> actual = hotelService.findAll();
        Assert.assertNotNull(actual);
    }

    @Test
    public void deleteTestPositive() {
        when(repository.delete(anyInt())).thenReturn(1);
        int count = hotelService.delete(1);
        Assert.assertEquals(1, count);
    }
}
