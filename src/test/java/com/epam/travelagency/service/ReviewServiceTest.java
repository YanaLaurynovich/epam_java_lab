package com.epam.travelagency.service;

import com.epam.travelagency.domain.Review;
import com.epam.travelagency.domain.User;
import com.epam.travelagency.repository.EntityRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ReviewServiceTest {

    @Mock
    private static EntityRepository<Review, Integer> repository;

    @Mock
    private static Review review;

    @InjectMocks
    private static ReviewService reviewService = new ReviewService(repository, review);


    @Test
    public void saveTestPositive() {
        when(repository.save(review)).thenReturn(1);
        Integer id = reviewService.save("adjsd", LocalDate.of(2020,12,12),
                3,3);
        Assert.assertEquals(Integer.valueOf(1), id);
    }

    @Test
    public void updateTestPositive() {
        when(repository.save(review)).thenReturn(1);
        Integer id = reviewService.update(1, "adjsd",
                LocalDate.of(2020,12,12),
                3,3);
        Assert.assertEquals(Integer.valueOf(1), id);
    }

    @Test
    public void findTestPositive() {
        when(repository.find(anyInt())).thenReturn(review);
        Review actual = reviewService.find(1);
        Assert.assertNotNull(actual);
    }

    @Test
    public void findAllTestPositive() {
        List<Review> reviewList = new ArrayList<>();
        when(repository.findAll()).thenReturn(reviewList);
        List<Review> actual = reviewService.findAll();
        Assert.assertNotNull(actual);
    }

    @Test
    public void deleteTestPositive() {
        when(repository.delete(anyInt())).thenReturn(1);
        int count = reviewService.delete(1);
        Assert.assertEquals(1, count);
    }
}
