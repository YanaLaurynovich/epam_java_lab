package com.epam.travelagency.service;

import com.epam.travelagency.domain.Tour;
import com.epam.travelagency.domain.TourType;
import com.epam.travelagency.repository.EntityRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TourServiceTest {
    @Mock
    private static EntityRepository<Tour, Integer> repository;

    @Mock
    private static Tour tour;

    @InjectMocks
    private static TourService tourService = new TourService(repository, tour);


    @Test
    public void saveTestPositive() {
        when(repository.save(tour)).thenReturn(1);
        Integer id = tourService.save("photo.jpg", LocalDate.of(2020, 12, 12),
                12, "Description.", 234, 2, 2, TourType.BUS.name());
        Assert.assertEquals(Integer.valueOf(1), id);
    }

    @Test
    public void updateTestPositive() {
        when(repository.save(tour)).thenReturn(1);
        Integer id = tourService.update(1, "photo.jpg", LocalDate.of(2020, 12, 12),
                12, "Description.", 234, 2, 2, TourType.BUS.name());
        Assert.assertEquals(Integer.valueOf(1), id);
    }

    @Test
    public void findTestPositive() {
        when(repository.find(anyInt())).thenReturn(tour);
        Tour actual = tourService.find(1);
        Assert.assertNotNull(actual);
    }

    @Test
    public void findAllTestPositive() {
        List<Tour> tourList = new ArrayList<>();
        when(repository.findAll()).thenReturn(tourList);
        List<Tour> actual = tourService.findAll();
        Assert.assertNotNull(actual);
    }

    @Test
    public void deleteTestPositive() {
        when(repository.delete(anyInt())).thenReturn(1);
        int count = tourService.delete(1);
        Assert.assertEquals(1, count);
    }
}
