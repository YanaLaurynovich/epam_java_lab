package com.epam.travelagency.repository;

import com.opentable.db.postgres.embedded.FlywayPreparer;
import com.opentable.db.postgres.junit.EmbeddedPostgresRules;
import com.opentable.db.postgres.junit.PreparedDbRule;
import org.junit.ClassRule;
import org.springframework.jdbc.core.JdbcTemplate;


public class EntityRepositoryTest {
    protected static JdbcTemplate jdbcTemplate;

    @ClassRule
    public static PreparedDbRule db = EmbeddedPostgresRules.preparedDatabase(
            FlywayPreparer.forClasspathLocation("db"));

    protected EntityRepositoryTest() {
        jdbcTemplate = new JdbcTemplate(db.getTestDatabase());
    }

}
