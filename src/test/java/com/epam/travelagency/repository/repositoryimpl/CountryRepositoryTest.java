package com.epam.travelagency.repository.repositoryimpl;

import com.epam.travelagency.domain.Country;
import com.epam.travelagency.repository.EntityRepositoryTest;
import com.epam.travelagency.repository.repositoryImpl.CountryRepository;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class CountryRepositoryTest extends EntityRepositoryTest {
    private static CountryRepository countryRepository;

    public CountryRepositoryTest() {
        countryRepository = new CountryRepository(jdbcTemplate);
    }

    @Test
    public void saveTestPositive() {
        Country country = new Country();
        country.setName("USA");
        int id = countryRepository.save(country);
        Assert.assertEquals(26, id);
    }

    @Test
    public void updateTestPositive() {
        Country country = new Country();
        country.setId(1);
        country.setName("Russia");
        int id = countryRepository.save(country);
        Assert.assertEquals(1, id);
    }

    @Test
    public void findTestPositive() {
        int id = 2;
        Country country = countryRepository.find(id);
        Country expectedCountry = new Country();
        expectedCountry.setId(2);
        expectedCountry.setName("Poland");
        Assert.assertEquals(expectedCountry, country);
    }

    @Test
    public void findAllTestPositive() {
        List<Country> countries = countryRepository.findAll();
        Assert.assertEquals(25, countries.size());
    }

    @Test
    public void deleteTestPositive() {
        int id = 26;
        int expectedCount = 1;
        int count = countryRepository.delete(id);
        Assert.assertEquals(expectedCount, count);
    }
}
