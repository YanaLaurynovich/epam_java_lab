package com.epam.travelagency.repository.repositoryimpl;

import com.epam.travelagency.domain.Tour;
import com.epam.travelagency.domain.TourType;
import com.epam.travelagency.repository.EntityRepositoryTest;
import com.epam.travelagency.repository.repositoryImpl.TourRepository;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Date;
import java.util.List;

public class TourRepositoryTest extends EntityRepositoryTest {
    private static TourRepository tourRepository;

    public TourRepositoryTest() {
        tourRepository = new TourRepository(jdbcTemplate);
    }


    @Test
    public void saveTestPositive() {
        Tour tour = new Tour();
        tour.setPhoto("noPhoto.jpg");
        tour.setDate(Date.valueOf("2020-12-12"));
        tour.setDuration(7);
        tour.setDescription("Test tour");
        tour.setCost(54.4);
        tour.setCountryId(1);
        tour.setHotelId(1);
        tour.setType(TourType.SHOP);
        int id = tourRepository.save(tour);
        Assert.assertEquals(1001, id);
    }

    @Test
    public void updateTestPositive() {
        Tour tour = new Tour();
        tour.setId(1);
        tour.setPhoto("noPhoto.jpg");
        tour.setDate(Date.valueOf("2018-03-26"));
        tour.setDuration(10);
        tour.setDescription("Sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut");
        tour.setCost(518);
        tour.setCountryId(30);
        tour.setHotelId(3);
        tour.setType(TourType.CHILD);
        int id = tourRepository.save(tour);
        Assert.assertEquals(1, id);
    }

    @Test
    public void findTestPositive() {
        int id = 1;
        Tour tour = tourRepository.find(id);
        Tour expectedTour = new Tour();
        expectedTour.setId(1);
        expectedTour.setPhoto("noPhoto.jpg");
        expectedTour.setDate(Date.valueOf("2018-03-26"));
        expectedTour.setDuration(10);
        expectedTour.setDescription("Sit amet risus. Donec egestas. Aliquam nec enim. Nunc ut");
        expectedTour.setCost(518);
        expectedTour.setCountryId(30);
        expectedTour.setHotelId(3);
        expectedTour.setType(TourType.CHILD);
        Assert.assertEquals(expectedTour, tour);
    }

    @Test
    public void findAllTestPositive() {
        List<Tour> tours = tourRepository.findAll();
        Assert.assertEquals(1000, tours.size());
    }

    @Test
    public void deleteTestPositive() {
        int id = 1001;
        int expectedCount = 1;
        int count = tourRepository.delete(id);
        Assert.assertEquals(expectedCount, count);
    }
}
