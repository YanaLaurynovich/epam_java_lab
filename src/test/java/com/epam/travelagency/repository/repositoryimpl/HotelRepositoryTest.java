package com.epam.travelagency.repository.repositoryimpl;

import com.epam.travelagency.domain.Feature;
import com.epam.travelagency.domain.Hotel;
import com.epam.travelagency.repository.EntityRepositoryTest;
import com.epam.travelagency.repository.repositoryImpl.HotelRepository;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


public class HotelRepositoryTest extends EntityRepositoryTest {

    private static HotelRepository hotelRepository;

    public HotelRepositoryTest() {
        hotelRepository = new HotelRepository(jdbcTemplate);
    }

    @Test
    public void saveTestPositive() {
        Hotel hotel = new Hotel();
        hotel.setName("TestHotel");
        hotel.setStars(3);
        hotel.setWebsite("hotel.by");
        hotel.setLatitude(23.4);
        hotel.setLongitude(54.3);
        List<Feature> features = new ArrayList<>();
        features.add(Feature.FREE_SMOKE);
        hotel.setFeatures(features);
        int id = hotelRepository.save(hotel);
        Assert.assertEquals(101, id);
    }

    @Test
    public void updateTestPositive() {
        Hotel hotel = new Hotel();
        hotel.setId(4);
        hotel.setName("Ac Company");
        hotel.setStars(3);
        hotel.setWebsite("hotel.by");
        hotel.setLatitude(23.4);
        hotel.setLongitude(54.3);
        List<Feature> features = new ArrayList<>();
        features.add(Feature.FREE_SMOKE);
        hotel.setFeatures(features);
        int id = hotelRepository.save(hotel);
        Assert.assertEquals(4, id);
    }

    @Test
    public void findTestPositive() {
        int id = 1;
        Hotel hotel = hotelRepository.find(id);
        Hotel expectedHotel = new Hotel();
        expectedHotel.setId(1);
        expectedHotel.setName("Adipiscing Limited");
        expectedHotel.setStars(4);
        expectedHotel.setWebsite("arcu@interdumCurabiturdictum.co.uk");
        expectedHotel.setLatitude(-27.02816);
        expectedHotel.setLongitude(30.88538);
        List<Feature> features = new ArrayList<>();
        features.add(Feature.FREE_PARKING);
        expectedHotel.setFeatures(features);
        Assert.assertEquals(expectedHotel, hotel);
    }

    @Test
    public void findAllTestPositive() {
        List<Hotel> hotels = hotelRepository.findAll();
        Assert.assertEquals(100, hotels.size());
    }

    @Test
    public void deleteTestPositive() {
        int id = 101;
        int expectedCount = 1;
        int count = hotelRepository.delete(id);
        Assert.assertEquals(expectedCount, count);
    }
}
