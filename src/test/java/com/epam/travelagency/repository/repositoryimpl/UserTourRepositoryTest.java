package com.epam.travelagency.repository.repositoryimpl;

import com.epam.travelagency.domain.Entity;
import com.epam.travelagency.domain.UserTourId;
import com.epam.travelagency.repository.EntityRepositoryTest;
import com.epam.travelagency.repository.repositoryImpl.UserTourRepository;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class UserTourRepositoryTest extends EntityRepositoryTest {
    private static UserTourRepository userTourRepository;

    public UserTourRepositoryTest() {
        userTourRepository = new UserTourRepository(jdbcTemplate);
    }


    @Test
    public void saveTestPositive() {
        UserTourId userTourId = new UserTourId();
        userTourId.setUserId(1);
        userTourId.setTourId(3);
        Entity<UserTourId> entity = new Entity<>();
        entity.setId(userTourId);
        UserTourId id = userTourRepository.save(entity);
        Assert.assertEquals(userTourId, id);
    }

    @Test
    public void findAllTestPositive() {
        List<Entity<UserTourId>> entities = userTourRepository.findAll();
        Assert.assertEquals(10, entities.size());
    }

    @Test
    public void deleteTestPositive() {
        int expectedCount = 1;
        UserTourId userTourId = new UserTourId();
        userTourId.setUserId(1);
        userTourId.setTourId(3);
        int count = userTourRepository.delete(userTourId);
        Assert.assertEquals(expectedCount, count);
    }

}
