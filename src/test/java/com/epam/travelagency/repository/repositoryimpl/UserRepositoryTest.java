package com.epam.travelagency.repository.repositoryimpl;

import com.epam.travelagency.domain.User;
import com.epam.travelagency.repository.EntityRepositoryTest;
import com.epam.travelagency.repository.repositoryImpl.UserRepository;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;


public class UserRepositoryTest extends EntityRepositoryTest {
    private static UserRepository userRepository;

    public UserRepositoryTest() {
        userRepository = new UserRepository(jdbcTemplate);
    }


    @Test
    public void saveTestPositive() {
        User user = new User();
        user.setLogin("Ksena");
        user.setPassword("qwerty");
        int id = userRepository.save(user);
        Assert.assertEquals(101, id);
    }

    @Test
    public void updateTestPositive() {
        User user = new User();
        user.setId(1);
        user.setLogin("mloding0@narod.ru");
        user.setPassword("VGx9JXk8");
        int id = userRepository.save(user);
        Assert.assertEquals(1, id);
    }

    @Test
    public void findTestPositive() {
        int id = 1;
        User user = userRepository.find(id);
        User expectedUser = new User();
        expectedUser.setId(1);
        expectedUser.setLogin("mloding0@narod.ru");
        expectedUser.setPassword("VGx9JXk8");
        Assert.assertEquals(expectedUser, user);
    }

    @Test
    public void findAllTestPositive() {
        List<User> users = userRepository.findAll();
        Assert.assertEquals(100, users.size());
    }

    @Test
    public void deleteTestPositive() {
        int id = 101;
        int expectedCount = 1;
        int count = userRepository.delete(id);
        Assert.assertEquals(expectedCount, count);
    }

}
