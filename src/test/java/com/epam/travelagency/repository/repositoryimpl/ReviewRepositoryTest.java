package com.epam.travelagency.repository.repositoryimpl;

import com.epam.travelagency.domain.Review;
import com.epam.travelagency.repository.EntityRepositoryTest;
import com.epam.travelagency.repository.repositoryImpl.ReviewRepository;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Date;
import java.util.List;

public class ReviewRepositoryTest extends EntityRepositoryTest {
    private static ReviewRepository reviewRepository;

    public ReviewRepositoryTest() {
        reviewRepository = new ReviewRepository(jdbcTemplate);
    }


    @Test
    public void saveTestPositive() {
        Review review = new Review();
        review.setText("saveTest");
        review.setDate(Date.valueOf("2012-10-10"));
        review.setUserId(2);
        review.setTourId(2);
        int id = reviewRepository.save(review);
        Assert.assertEquals(1001, id);
    }

    @Test
    public void updateTestPositive() {
        Review review = new Review();
        review.setId(1);
        review.setText("ante ipsum primis in faucibus orci luctus et ultrices posuere");
        review.setDate(Date.valueOf("2018-11-09"));
        review.setUserId(5);
        review.setTourId(50);
        int id = reviewRepository.save(review);
        Assert.assertEquals(1, id);
    }

    @Test
    public void findTestPositive() {
        int id = 1;
        Review review = reviewRepository.find(id);
        Review expectedReview = new Review();
        expectedReview.setId(1);
        expectedReview.setText("ante ipsum primis in faucibus orci luctus et ultrices posuere");
        expectedReview.setDate(Date.valueOf("2018-11-09"));
        expectedReview.setUserId(5);
        expectedReview.setTourId(50);
        Assert.assertEquals(expectedReview, review);
    }

    @Test
    public void findAllTestPositive() {
        List<Review> reviews = reviewRepository.findAll();
        Assert.assertEquals(1000, reviews.size());
    }

    @Test
    public void deleteTestPositive() {
        int id = 1001;
        int expectedCount = 1;
        int count = reviewRepository.delete(id);
        Assert.assertEquals(expectedCount, count);
    }
}
